{ config, lib, pkgs, inputs, ... }:

{
  services = {
    xserver = {
      enable = true;

      layout = "us";
      xkbOptions = "eurosign:e";

      libinput = {
        enable = true;
      };
    };
  };
}