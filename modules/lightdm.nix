{ config, lib, pkgs, inputs, ... }:

{
  services = {
    xserver = {
      displayManager = {   
        defaultSession = "none+bspwm";
        lightdm = {
          enable = true;
          background = ../static/pictures/wallpaper.jpg;
        };
      };
    };
  };
}