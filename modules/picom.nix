{ pkgs, ... }:

{ 
  services.picom = {
    enable = true;
    backend = "glx";
    fade = true;
    fadeDelta = 5;
    shadow = true;
    shadowOpacity = 0.35;
    #menuOpacity = "0.95";
    vSync = true;
  };
}
