{ config, lib, pkgs, inputs, ... }:

{
  sound = {
    enable = true;
    mediaKeys = {
      enable = true;
    };
  };
  
  hardware = {
    pulseaudio = {
      enable = true;
      package = pkgs.pulseaudioFull;
      extraConfig = ''
        load-module module-switch-on-connect
      '';
    };
  };
}