{ config, lib, pkgs, ... }:

{
  programs.dconf.enable = true;

  services.xserver.windowManager.bspwm = {
    enable = true;
  };
}