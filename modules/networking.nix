{ config, lib, pkgs, inputs, ... }:

{
  networking = {
    wireless.iwd.enable = true;

    networkmanager = {
      enable = true;
      
      wifi.backend = "iwd";
      insertNameservers = [ "84.200.69.80" "8.8.8.8" ];
    };
  };
}