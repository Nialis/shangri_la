{ config, lib, pkgs, inputs, ... }:

{
  fonts.fonts = with pkgs; [
    font-awesome
    corefonts
    (nerdfonts.override {
      fonts = [
        "JetBrainsMono"
      ];
    })
  ];
}