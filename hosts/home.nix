{ config, lib, pkgs, user, ... }:

{
  imports =
    [(import ../apps)] ++
    [(import ../static)];

  home = {
    username = "${user}";
    homeDirectory = "/home/${user}";

    stateVersion = "22.05";
  };

  programs = {
    home-manager.enable = true;
  };

  xsession = {
    enable = true;
    numlock.enable = true;
  };

  gtk = {
    enable = true;
    theme = {
      name = "Dracula";
      package = pkgs.dracula-theme;
    };
    cursorTheme = {
      name = "Dracula-cursors";
      package = pkgs.dracula-theme;
      size = 16;
    };
    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.papirus-icon-theme;
    };
    font = {
      name = "JetBrainsMono Nerd Font Mono Medium";  
    };
  };
}
