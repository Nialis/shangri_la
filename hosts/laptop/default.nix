{ config, pkgs, lib, user, ... }:

{
  imports =
    [(import ./hardware-configuration.nix)];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;

    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
  };

  services = {
    blueman.enable = true;
  };
}
