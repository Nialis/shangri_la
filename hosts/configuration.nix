{ config, lib, pkgs, inputs, user, location, ... }:

{
  imports =
    (import ../modules);

  environment = {
    sessionVariables = {
      TERMINAL = "alacritty";
      EDITOR = "nvim";
      VISUAL = "nvim";
      SXHKD_SHELL = "/bin/sh";
      PATH = "$PATH:/home/${user}/Scripts";
    };
    systemPackages = with pkgs; [
      killall
      nano
      pciutils
      usbutils
      wget
      xclip
      xterm
      xorg.xev
      arandr
    ];
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };
}
