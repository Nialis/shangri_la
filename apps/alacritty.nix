{ pkgs, ... }:

{
  programs = {
    alacritty = {
      enable = true;
      settings = {
        font = rec {
          normal.family = "JetBrainsMono Nerd Font Mono";
          bold = { style = "Bold"; };
          italic = { style = "Italic"; };
          size = 7;
        };
      };
    };
  };
}