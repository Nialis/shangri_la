{ pkgs, ... }:

{
  services.polybar = {
    enable = true;
    package = pkgs.polybar.override {
      alsaSupport = true;
      pulseSupport = true;
    };
    script = "polybar primary &";
    config = {
      "bar/primary" = {
        font-0 = "JetBrainsMonoNerdFont:size=11:weight=bold";

        width = "100%";
        height = 25;

        background = "#000";
        foreground = "#ccffffff";

        modules-left = "bspwm";
        modules-right = "cpu memory volume battery date";
        
        tray-position = "right";
        tray-detached = "false";

        wm-restack = "bspwm";
      };

      "module/volume" = {
        type = "internal/pulseaudio";
        interval = 2;
        use-ui-max = "false";
        format-volume = "<ramp-volume>  <label-volume>";
        label-muted = "  muted";
        label-muted-foreground = "#66";

        ramp-volume-0 = "";
        ramp-volume-1 = "";
        ramp-volume-2 = "";

        click-right = "${pkgs.pavucontrol}/bin/pavucontrol";
      };

      "module/memory" = {
        type = "internal/memory";
        format = "<label>";
        format-foreground = "#999";
        label = "  %percentage_used%% ";
      };

      "module/cpu" = {
        type = "internal/cpu";
        interval = 1;
        format = "<label>";
        format-foreground = "#999";
        label = "  %percentage%% ";
      };

      "module/battery" = {
        type = "internal/battery";
        battery = "BAT1";
        full-at = 98;

        label-full = "  %percentage%% ";
        label-charging = "  %percentage%% ";
        label-discharging = "  %percentage%% ";
      };

      "module/date" = {
        type = "internal/date";
        date = "%%{F#999}%d-%m-%Y%%{F-} %%{F#fff}%H:%M%%{F-}";
      };

      "module/bspwm" = {
        type = "internal/bspwm";
        pin-workspace = true;

        ws-icon-0 = "1;";
        ws-icon-1 = "2;";
        ws-icon-2 = "3;";
        ws-icon-3 = "4;";
        ws-icon-4 = "5;";
        ws-icon-5 = "6;";
        ws-icon-6 = "7;";
        ws-icon-7 = "8;";
        ws-icon-8 = "9;";
        ws-icon-10 = "10;";
        ws-icon-11 = "11;";
        ws-icon-12 = "12;";
        #ws-icon-default = "";

        format = "<label-state> <label-mode>";

        label-dimmed-underline = "#ccffffff";

        #label-focused = "%icon%";
        label-focused-foreground = "#fff";
        label-focused-background = "#773f3f3f";
        label-focused-underline = "#c9665e";
        #label-focused-font = 4;
        #label-focused-padding = 2;
      };
    };
  };
}
