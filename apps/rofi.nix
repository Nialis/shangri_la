{ pkgs, ... }:

{
  programs = {
    rofi = {
      enable = true;
      font = "JetBrainsMono Nerd Font Mono Medium 12";
    };
  };
}