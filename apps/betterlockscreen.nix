{ pkgs, ... }:

{
  services = {
    betterlockscreen = {
      enable = true;
      arguments = ["--lock"];
    };
  };
}
