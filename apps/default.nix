{ pkgs, ... }:

{
  imports = [
    ./alacritty.nix
    ./sxhkd.nix
    ./bspwm.nix
    ./polybar.nix
    ./neovim.nix
    ./tmux.nix
    ./rofi.nix
    ./betterlockscreen.nix
    ./flameshot.nix
  ];

  home.packages = with pkgs; [
    btop
    xplr
    feh
    mpv
    pavucontrol
    libnotify
    firefox
    vscode
    git
    qutebrowser
    asdf-vm
    xfce.thunar
    xtitle
  ];
}