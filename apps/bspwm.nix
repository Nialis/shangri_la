{ pkgs, ... }:

{
  xsession.windowManager.bspwm = {
    enable = true;
    extraConfig = ''
      bspc monitor -d 1 2 3 4 5 6 7 8 9 10 11 12

      bspc config focus_follows_pointer    true
      bspc config borderless_monocle       true
      bspc config gapless_monocle          true
      bspc config single_monocle           true
      bspc config window_gap               0

      # Autostart
      #killall -q polybar &
      #polybar primary &
    '';
  };
}
