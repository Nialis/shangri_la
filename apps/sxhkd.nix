{ pkgs, ... }:

{
  services = {
    sxhkd = {
      enable = true;
      keybindings = {
        # Apps
        "super + c" =
          "code";
        "super + f" =
          "qutebrowser";
        "super + Return" =
          "alacritty";
        "super + shift + Return" =
          "alacritty -e tmux";
        "super + e" =
          "alacritty -e xplr";
        "super + shift + e" =
          "thunar";

        # WM
        ## Kill and close
        "super + {q,k}" =
          "bspc node -{c,k}";
        "super + shift + Escape" =
          "bspc quit";
        "super + shift + r" =
          "bspc wm -r && killall sxhkd && sxhkd &";
        "super + shift + l" =
          "power_menu";

        ## Node placement
        "super + {_,shift +}{h,l,j,k}" =
          "bspc node -{f,s} {west,east,north,south}";
        "super + {_,shift +} {1-9,0}" =
          "bspc {desktop -f,node -d} '{1-9,10}'";
        "super + shift + space" =
          "bsp_toggle_tiled";
        "super + BackSpace" =
          "bspc node focused -g hidden=on";
        "super + shift + BackSpace" =
          "bsp_hidden";

        # Special
        "XF86MonBrightnessDown" =
          "light -U  5";
        "XF86MonBrightnessUp" =
          "light -A 5";
        "XF86AudioLowerVolume" =
          "pactl -- set-sink-volume 0 -10%";
        "XF86AudioRaiseVolume" =
          "pactl -- set-sink-volume 0 +10%";
        "XF86AudioMute" =
          "pactl list sinks | grep -q Mute:.no && pactl set-sink-mute 0 1 || pactl set-sink-mute 0 0";
        "XF86AudioMicMute" =
          "pactl set-source-mute 1 toggle";
        "Print" =
          "flameshot full --path ~/Pictures/Screens";
        "shift + Print" =
          "flameshot gui --path ~/Pictures/Screens";

      };
    };
  };
}
