{ pkgs, lib, ... }:

{
  home.activation = {
    scripts = lib.hm.dag.entryAfter ["writeBoundary"] ''
      $DRY_RUN_CMD rm -rf $VERBOSE_ARG ~/Scripts
      $DRY_RUN_CMD cp -r $VERBOSE_ARG ${./scripts} ~/Scripts
      $DRY_RUN_CMD find $VERBOSE_ARG ~/Scripts -type d -exec chmod 744 {} \;
      $DRY_RUN_CMD find $VERBOSE_ARG ~/Scripts -type f -exec chmod 644 {} \;
      $DRY_RUN_CMD chmod +x $VERBOSE_ARG ~/Scripts/*
    '';
    pictures = lib.hm.dag.entryAfter ["writeBoundary"] ''
      $DRY_RUN_CMD mkdir -p $VERBOSE_ARG ~/Pictures
      $DRY_RUN_CMD cp -r $VERBOSE_ARG ${./pictures/wallpaper.jpg} ~/Pictures/wallpaper.jpg
      $DRY_RUN_CMD find $VERBOSE_ARG ~/Pictures -type d -exec chmod 744 {} \;
      $DRY_RUN_CMD find $VERBOSE_ARG ~/Pictures -type f -exec chmod 644 {} \;
    '';
  };
}